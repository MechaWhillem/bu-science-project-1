﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engrenage : MonoBehaviour
{

    public int speed;

    private void Update()
    {
        transform.Rotate(0, 0, speed * Time.deltaTime);
    }   
}
