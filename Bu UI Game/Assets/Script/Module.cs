﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Module : MonoBehaviour
{
    public Slider slide1, slide2, slide3;
    public int value1, value2, value3;
    public Animator error, valide,module,batterie , giro;
    private bool haveActive = false;
    public GameObject spote , lightScene;
    
 

    public void ButtonPress()
    {
        if (slide1.value == value1 && slide2.value == value2 && slide3.value == value3 && haveActive == false)
        {
            valide.SetTrigger("Start");
            Debug.Log("vraie");
            spote.SetActive(true);
            module.SetTrigger("Start");
            batterie.SetTrigger("Start");

            haveActive = true;
        } else
        {
            error.SetTrigger("Start");
            Debug.Log("faux");
        }
    }

    public void buttonRouge ()
    {
        if (haveActive == true)
        {
            giro.SetTrigger("Start");
            lightScene.SetActive(false);
        }
    }

}
