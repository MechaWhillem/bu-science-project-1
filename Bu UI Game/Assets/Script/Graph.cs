﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Graph : MonoBehaviour
{
    public Transform spawn;
    
    [Range(0, 1)]
    public float intensity=1f;

    public Slider easeTypeSlider;
    public Slider frequenceSlider;
    public Slider intensitySlider;
    public TrailRenderer trail;

    Tween myTween;
   

    // Start is called before the first frame update
    void Start()
    {
       
        StartCoroutine(MovementParticule());

    }

    // Update is called once per frame
    void Update()
    {
      
           



            if (myTween != null)
                switch (easeTypeSlider.value)
                {
                    case 0:
                        myTween.SetEase(Ease.InCubic);
                        break;
                    case 1:
                        myTween.SetEase(Ease.InBounce);
                        break;
                    case 2:
                        myTween.SetEase(Ease.Flash);
                        break;
                    case 3:
                        myTween.SetEase(Ease.InOutExpo);
                        break;

                    case 4:
                        myTween.SetEase(Ease.InOutCirc);
                        break;
                }


           

            transform.position += Vector3.right * Time.deltaTime;
            if (transform.position.x > -0.310)
            {
                StartCoroutine(StopTrailEmitting());

            }

        }


        IEnumerator StopTrailEmitting()
        {

            trail.emitting = false;
            yield return new WaitForSeconds(0.03f);
            transform.position = new Vector3(-0.311f, 1.023f, -0.631f);
            transform.position = spawn.position;

            yield return new WaitForSeconds(0.005f);
            trail.emitting = true;


        }

        IEnumerator MovementParticule()
        {

            while (true)
            {

                myTween = transform
                  .DOMoveZ(-0.212f  - 0.206f/(6/(6 - intensitySlider.value)), 0.5f / (frequenceSlider.value))
                 
                  .SetEase(Ease.InOutQuad);

          

            yield return new WaitUntil(() => !myTween.active);


            myTween = transform
                 .DOMoveZ(-0.624f + 0.206f / (6/(6 - intensitySlider.value)), 0.5f / (frequenceSlider.value))
                
                 .SetEase(Ease.InOutQuad);
           

            yield return new WaitUntil(() => !myTween.active);
        }
        }
    }
    
