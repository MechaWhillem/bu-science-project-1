﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fusee : MonoBehaviour {

    private bool firstfly = false;
    private bool fly = false;
    public GameObject text1, text2, text3, text4,particule,flamme;
    public int speed;
    public Animator shake;

	void Update () {
        if (Input.GetKey(KeyCode.Space))
        {
            StartCoroutine("Fly");
        }

        if (fly == true)
        {
            transform.Translate(Vector3.up * Time.deltaTime * speed, Space.World);
            speed = speed + 1;
        }
	}

    IEnumerator Fly ()
    {
        if (firstfly == false)
        {
            firstfly = true;
            particule.SetActive(true);
            text1.SetActive(true);
            shake.SetTrigger("Start");
            yield return new WaitForSeconds(1f);
            text1.SetActive(false);
            text2.SetActive(true);
            yield return new WaitForSeconds(1f);
            text2.SetActive(false);
            text3.SetActive(true);
            yield return new WaitForSeconds(1f);
            text3.SetActive(false);
            text4.SetActive(true);
            yield return new WaitForSeconds(1f);
            shake.SetTrigger("End");
            text4.SetActive(false);
            flamme.SetActive(true);
            fly = true;
        }
    }
}
