﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lookAt : MonoBehaviour {

    public GameObject focus;

	void Update () {
        transform.LookAt(focus.transform.position);
	}
}
